from django.shortcuts import render

from django.http import HttpResponse
from django.views import View

# Create your views here.

class Principal(View):
    
    def get(self, request):
        
        datos={
            
            'titulo': 'General',
            'encabezado': 'Mapeo de la Movilidad Domiciliaria',
            'bienvenida': 'Seguridad para todos'
        }
        
        return render(request, 'principal/Tprincipal.html', datos)
    
    