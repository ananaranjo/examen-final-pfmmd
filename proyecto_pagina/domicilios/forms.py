from django.forms import BooleanField, CharField, Form, IntegerField 

class Encuestas_cliente(Form):
    nombre=CharField(max_length=50)
    identificacion= IntegerField(max_value=15)
    numero_domimcilio=  IntegerField()
    observaciones= CharField(max_length=200)
    
class Encuestas_domicilio(Form):
    nombre=CharField(max_length=50)
    identificacion= IntegerField(max_value=15)
    numero_domimcilio=  IntegerField()
    observaciones= CharField(max_length=200)
    peligro= BooleanField()
    
class Registro(Form):
    nombre= CharField(max_length=50)
    correo_electronico= CharField(max_length=50)
    identificacion= IntegerField()
    edad= IntegerField(min_value=1, max_value=3)
    numero= IntegerField()
    direccion= CharField(max_length=50)