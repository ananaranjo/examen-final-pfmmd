from django.db.models import *

# Create your models here.

class Domiciliario(Model):
    nombre= CharField(max_length=100)
    identificacion= FloatField(max_length=3)
    telefono= FloatField(max_length=10)
    hora= TimeField()
    direccion= CharField(max_length=20)
    
class Cliente(Model):
    nombre= CharField(max_length=100)
    cc= IntegerField()
    telefono= FloatField(max_length=10)
    direccion= CharField(max_length=50)
    
class Encuestas_cliente(Model):
    nombre= CharField(max_length=100)
    identificacion_cliente= ForeignKey(Cliente, on_delete=PROTECT)
    numero_domicilio= IntegerField()
    observaciones= CharField(max_length=200)
    
class Encuestas_domiciliario(Model):
    nombre= CharField(max_length=100)
    identificacion_domiciliario= ForeignKey(Domiciliario, on_delete=PROTECT)
    numero_domicilio= IntegerField()
    observaciones= CharField(max_length=200)
    peligro= BooleanField()
    
class Entrega(Model):
    eficiencia= CharField(max_length=100)
    velocidad= CharField(max_length=3)
    estado_del_producto= CharField(max_length=40)
    observaciones= CharField(max_length=200)
    
class Propinas(Model):
    frecuencia= CharField(max_length=10)
    motivacion= CharField(max_length=25)
    cantidad= FloatField(max_length=15)
    
class Local_comercial(Model):
    nombre= CharField(max_length=50)
    telefono= FloatField(max_length=10)
    direccion= CharField(max_length=50)
    
class Mantenimiento(Model):
    causa= CharField(max_length=100)
    costo= FloatField(max_length=25)
    frecuencia= CharField(max_length=25)
    garantia= CharField(max_length=25)
    
class Nivel_riesgo(Model):
    zona_peligrosa= CharField(max_length=30)
    calle= CharField(max_length=25)
    localidad= CharField(max_length=20)
    hora= TimeField()
